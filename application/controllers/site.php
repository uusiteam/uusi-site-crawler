<?php
  
class site extends CI_Controller {



   function __construct()
       {
     		parent::__construct();
			$this->load->helper(array('form', 'url'));
			$this->load->model('corporate_model');
	   }

	
	
	function index()
    {
		$this->load->view('add_recruiter');
    }
	
	function add_job()
	{
		$this->load->library('form_validation');

		// field name, error message, validation rules
		// $this->form_validation->set_rules('reference_number', 'Refrence Number', 'trim|required');
		// $this->form_validation->set_rules('title', 'Job Title', 'trim|required');		
		// $this->form_validation->set_rules('description', 'Job Description', 'trim|required');
		// $this->form_validation->set_rules('requirements', 'Job Requirements', 'trim|required');
		
		if( $this->form_validation->run() != true )
		{
				$this->load->view('add_job'); 
		}
		else
		{
			$this->corporate_model->add_jobs();	
		}
	}
	
	function add_recruiter()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('company_name', 'Name', 'trim|required');
		$this->form_validation->set_rules('contact_details', 'Contact Information', 'trim|required');		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|unique[corporate_recruiter.email]');
		if( $this->form_validation->run() != true )
		{
				$this->corporate_model->generate_report();	
				//$this->load->view('add_recruiter'); 
		}
		else
		{
			$this->corporate_model->generate_report();	
		}
	
	}
	
	public function send_reminder()
	{
			$data = $this->corporate_model->date_diff();
			$i = 0;
			for($i; $i < count($data); $i++)
			{
				if($data[$i]['date'] >= 13)
				{
					die('hi');
				}
			}
			
			if($data['date'] >= 13)
			{
			die('hi');
			$this->load->library('email');
			
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'ssl://smtp.gmail.com';
			$config['smtp_user'] = 'webmaster@uusi.co.za';
			$config['smtp_pass'] = '3UK^$Zo&WY3z';
			$config['smtp_port'] = '465';
			$config['smtp_timeout'] = '7';
			$config['mailtype']  ='html'; 
			$config['charset']   = 'iso-8859-1';
			$this->email->set_newline("\r\n");
			$job_id = 1;
			$data = $this->corporate_model->get_email_details($job_id);
			$this->email->initialize($config);		
			
			$message = 'Dear , '.$data['company_name']."\r\n"; ;
			$message .= 'This is just a friendly reminder, that your job'.$data['reference_number'].' "'.$data['title'].' will expire soon.'."\r\n"; 
			$message .= 'To reactivate this job listing for another 30 days, please click this '. anchor('site/activate_job/'.$job_id,'link' )."\r\n"; 
			$message .= 'Looking forward to seeing you online !'."\r\n";
			$message .= ' '."\r\n";
			$message .= 'Uusi Team';
			
			
			$this->email->from('webmaster@uusi.co.za', 'Uusi Webmaster');
			$this->email->to($data['email']);
			$this->email->subject('Uusi Job Activation');
			$this->email->message($message);		
			$this->email->send();
			
			if($this->email->send())
			 {
				echo 'Email sent.';
			 }
			 else
			{
				show_error($this->email->print_debugger());
			}
			}
		
	}
	
	public function activate_job()
	{
		$this->load->view('thank_you');
		$job_id = $this->uri->segment(3, 0);
		$this->corporate_model->set_active_job($job_id);
	}
}