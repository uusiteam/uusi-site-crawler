<?php

class Upload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	function index()
	{
		$this->load->view('upload_form', array('error' => ' ' ));
	}

	function do_upload()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'csv';
		$config['max_size']	= '100';
		$config['overwrite'] = TRUE;
		$config['file_name'] = 'placer_name_jobs.csv';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			$this->load->view('upload_success', $data);
		}

		$this->read_csv($config['upload_path'] . $config['file_name']);
	}

	function read_csv($file_path)
	{
		$this->load->library('getcsv');

		$data = $this->getcsv->set_file_path($file_path)->get_array();

		foreach($data as $job)
		{
			$job['date_listed'] = date('Y-m-d H:i:s', time()+7200);
			$job['placer'] = 5;
			$temp[] = $job;
		}
		$this->load->model('job_model', 'job');
		$rows_affected = $this->job->insert_batch($temp);

		echo $rows_affected . ' job(s) was successfully added!';
	}
}

?>