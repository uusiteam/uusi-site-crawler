<?php
	define('time_last',time()+60*60*24*30);
	$title='Vanilla - Order ';
 
	$send=$_GET['send'];
	
	include('files/parts.php');
	include('about_us.php');
?>
	
	<div id="left_content">
		<h1>Online order form...</h1>
		<p>If you are not sure which products you wish to order, have a look at our Vanilla Services. If you need assitance in choosing the right product, contact Vanilla Support.</p>
		<p>To place an order, simply enter your contact details below. </p>
		<p>Alternatively you can download on of our order forms and fax it to +27 21 409 7050 or email to 
			<script language=javascript>
			<!--
			var contact = "contactus at vanilla.co.za"
			var email = "contactus"
			var emailHost = "vanilla.co.za"
			document.write("<a href=" + "mail" + "to:" + email + "@" + emailHost+ ">" + contact + "</a>")
			//-->
			</script></p>
		<p><ul>
			<li><a onclick="window.open(this.href,'newwin'); return false;" href="files/VANILLA_DSL_OrderForm.pdf"><strong>DSL Order form</strong></a> (.pdf, 28 KB).</li>
			<li><a onclick="window.open(this.href,'newwin'); return false;" href="files/VANILLA_DSL_OrderForm.doc"><strong>DSL Order form</strong></a> (.doc, 44 KB).</li>
			<li><a onclick="window.open(this.href,'newwin'); return false;" href="files/VANILLA_SAARP_DIALUP.pdf"><strong>SAARP Order form</strong></a> (.pdf, 19.7 KB).</li>
		</ul></p>
		<p>After you have signed up, one of our friendly sales consultants will contact you.</p>
		<br>
	<h4>Send us an email from this page by filling out the form below:</h4>
	<form action="verify.php" method="post" id="order_Form" >
		<div class="checkbox">
			<table>
				<tr>
					<td style="width: 200px;"><label for="adsl_usage" >ADSL usage account:</label></td>
					<td><input type="checkbox" name="adsl_usage" id="adsl_usage"  onclick="return switchMain();"  <?php if($order1['adsl_usage']=='on'){ ?> checked  <?php } ?>/></td>
					<td>Check the box for detailed information</td>
				</tr>		
			</table>
		</div>
		<div id="ads" style="display:  <?php if($order1['adsl_usage']=='on'){ ?> ; <?php }else { ?> none; <?php } ?>; margin-left: 203px; width: 630px">
			<div class="radio" style="border:1px solid gray;">
				<table>
					<tr>
						<td>&nbsp;<input type="radio" name="adsl_type" id="adsl_type" value="shaped_combo" <?php if($order1['adsl_type']=='shaped_combo'){ ?> checked  <?php } ?>/></td>
						<td><label for="adsl_shaped">Metered Shaped Combo (R49 per GB, or 7c per MB) <br> IS and SAIX network access. Standard size is 1GB prepaid with 3GB provisioned for over usage.</label></td>
					</tr>
					
					<tr>
					<td>&nbsp;<input type="radio" name="adsl_type" id="adsl_type" value="unshaped_combo" <?php if($order1['adsl_type']=='unshaped_combo'){ ?> checked  <?php } ?>/></td>
						<td><label for="adsl_unshaped">Metered super duper triple redundant Unshaped Combo (R87 per GB, or 12c per MB) <br> IS, SAIX and MTN network access. Standard size is 1GB prepaid with 3GB provisioned for over usage.</label></td>
					</tr>
					
					<tr>
					<td>&nbsp;<input type="radio" name="adsl_type" id="adsl_type" <?php if($order1['adsl_type']=='on'){ ?> checked  <?php } ?>/></td>
						<td><label for="adsl_fixedprice">Fixed Price Access: </label>
							<select name="adsl_fixedprice" style="width: 300px;">
									<option value="(select)"> Select a fixed price account type </option>
									<option value="384_kbps" <?php if($order1['adsl_fixedprice']=='Basic uncapped 384kbps'){ ?> selected  <?php } ?>> Basic Uncapped 384 kbps - R199 per month</option>
									<option value="512_kbps" <?php if($order1['adsl_fixedprice']=='Basic uncapped 512 kbps'){ ?> selected  <?php } ?>> Basic Uncapped 512 kbps - R299 per month</option>
									<option value="4096kbps" <?php if($order1['adsl_fixedprice']=='Basic uncapped 4096 kbps'){ ?> selected  <?php } ?>> Basic Uncapped 4096 kbps - R499 per month</option>
									<option value="1gb_local" <?php if($order1['adsl_fixedprice']=='1gb_local'){ ?> selected  <?php } ?>> 10GB Local (First 1GB International) - R69 </option>
									<option value="2gb_local" <?php if($order1['adsl_fixedprice']=='2gb_local'){ ?> selected  <?php } ?>> 30GB Local (First 2GB International) - R138 </option>
						</select>
						<br>IS networks access only. Use as much as you can. Prepaid only, no refunds.
				</table>
			</div>
		</div>
		<div class="checkbox">
			<table>
				<tr>
					<td style="width: 200px;"><label for="adsl_line_type">ADSL line:</label></td>
					<td><input type="checkbox" name="adsl_line_type" id="adsl_line_type" <?php if($order1['adsl_line_type']=='on'){ ?> checked  <?php } ?>/></td>
					<td><select name="adsl_line" style="width: 200px;">
							<option value="(select)"> Select ADSL line speed </option>
							<option value="384" <?php if($order1['adsl_line']=='384'){ ?> selected  <?php } ?>> 384 kbps - R149 per month </option>
							<option value="512" <?php if($order1['adsl_line']=='512'){ ?> selected  <?php } ?>> 512 kbps - R325 per month </option>
							<option value="4096" <?php if($order1['adsl_line']=='4096'){ ?> selected  <?php } ?>> 4096 kbps - R410 per month </option>			
						</select>
					</td>
				</tr>
			
				<tr>
					<td style="width: 200px;"><label for="dial_up_type">Dial-up internet:</label></td>
					<td><input type="checkbox" name="dial_up_type" id="dial_up_type" <?php if($order1['dial_up_type']=='on'){ ?> checked  <?php } ?>/></td>
					<td><select name="dial_up" style="width: 200px;">
							<option value="(select)"> Select Dial-up Option </option>
							<option value="r69" <?php if($order1['dial_up']=='r69'){ ?> selected  <?php } ?>> R69 per month </option>
							<option value="r690" <?php if($order1['dial_up']=='r690'){ ?> selected  <?php } ?>> R690 per annum </option>	
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="email_account">Email Account:</label></td>
					<td><input type="checkbox" name="email_account_type" id="email_account_type" <?php if($order1['email_account_type']=='on'){ ?> checked  <?php } ?>/></td>
					<td><select name="email_account" style="width: 200px;">
							<option value="(select)"> Select Email Option </option>
							<option value="r20" <?php if($order1['email_account']=='r20'){ ?> selected  <?php } ?>> R20 per account per month </option>
							<option value="r200" <?php if($order1['email_account']=='r200'){ ?> selected  <?php } ?>> R200 per account per annum </option>	
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="domain_type">Domain hosting:</label></td>
					<td><input type="checkbox" name="domain_type" id="domain_type" <?php if($order1['domain_type']=='on' || $domains){ ?> checked  <?php } ?>/></td>
					<td><textarea rows="3" id="domain" name="domain" style="border: 1px solid #CCCCCC;font-family: Verdana, Arial, Helvetica, sans-serif;padding: 0.2em;width: 195px;margin-left:0px;"><?php if($domains) {foreach($domains as $val){echo $val.", ";}}else{echo $order1['domain'];}?></textarea></td>
					<td>Include all .co.za, org.za, .com, .net, .org, .biz, .info, .com.au, .co.uk, etc<br>Please do not use spaces, commas, $,&amp;,%,^,+,@,#,&amp;,*,~,",/,\\,ect.</td>
				</tr>
			</table>		
			<table>	
				<tr>
					<td style="width: 200px;"><label for="other_reqiest_type">Other request:</label></td>
					<td><input type="checkbox" name="other_reqiest_type" id="other_reqiest_type" <?php if($order1['other_reqiest_type']=='on'){ ?> checked  <?php } ?>/></td>
					<td><textarea rows="3" name="other_reqiest" id="other_reqiest" style="border: 1px solid #CCCCCC;font-family: Verdana, Arial, Helvetica, sans-serif;padding: 0.2em;width: 400px;margin-left:0px;"><?php echo $order1['other_reqiest'];?></textarea></td>
				</tr>
			</table>
		</div>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2"><h4>Communication Information</h4></td>
				</tr>
				<tr>
					<td><label for="phone">Telephone Number:</label></td>
					<td><input type="text" name="phone" id="phone" value="<?php echo $order2['phone'];?>"/></td>
				</tr>
				<tr>
					<td><label for="fax">Fax Number:</label></td>
					<td><input type="text" name="fax" id="fax" value="<?php echo $order2['fax'];?>"/></td>
				</tr>
				<tr>
					<td><label for="mail">Email Address*:</label></td>
					<td><input type="text" name="mail" id="mail" value="<?php echo $order2['mail'];?>"/></td>
				</tr>
				<tr>
					<td><label for="physical_adress">Physical Address:</label></td>
					<td><input type="text" name="physical_adress" id="physical_adress" value="<?php echo $order2['physical_adress'];?>"/></td>
				</tr>
				<tr>
					<td><label for="post_adress">Postal Address:</label></td>
					<td><input type="text" name="post_adress" id="post_adress" value="<?php echo $order2['post_adress'];?>"/></td>
				</tr>
				<tr>
					<td colspan="2"><h4>Contact information</h4></td>
				</tr>
				<tr>
					<td><label for="title">Title:</label></td>
					<td><input type="text" name="titles" id="titles" value="<?php echo $order2['titles'];?>"/></td>
				</tr>
				<tr>
					<td><label for="initials">Initials</label></td>
					<td><input type="text" name="initials" id="initials" value="<?php echo $order2['initials'];?>"/></td>
				</tr>
				<tr>
					<td><label for="first_name">First names:</label></td>
					<td><input type="text" name="first_name" id="first_name" value="<?php echo $order2['first_name'];?>"/></td>
				</tr>
				<tr>
					<td><label for="surname">Surname:</label></td>
					<td><input type="text" name="surname" id="surname" value="<?php echo $order2['surname'];?>"/></td>
				</tr>
				<tr>
					<td><label for="department">Contact department:</label></td>
					<td><input type="text" name="department" id="department" value="<?php echo $order2['department'];?>"/></td>
				</tr>
				<tr>
					<td colspan="2"><h4>Aditional information for Carbon Copies</h4></td>
				</tr>
				<tr>
					<td><label for="copy_invoices">Copy invoices to email address(es):</label></td>
					<td><input type="text" name="copy_invoices" id="copy_invoices" value="<?php echo $order2['copy_invoices'];?>"/></td>
				</tr>
				<tr>
					<td><label for="copy_statements">Copy statements to email address(es):</label></td>
					<td><input type="text" name="copy_statements" id="copy_statements" value="<?php echo $order2['copy_statements'];?>"/></td>
				</tr>
				<tr>
					<td colspan="2"><h4>Billing Entity</h4></td>
				</tr>
				<tr>
					<td style="width:225px;"><label for="debtor_name">Debtor Name*:</label></td>
					<td>
						<input type="text" name="debtor_name" id="debtor_name" value="<?php echo $order3['debtor_name']; ?>" />
						<em>this is who the account will come addressed to</em>
					</td>
				</tr>
				<tr>
					<td><label for="reg_number">Registration number*:</label></td>
					<td>
						<input type="text" name="reg_number" id="reg_number" value="<?php echo $order3['reg_number']; ?>" />
						<em>if company</em>
					</td>
				</tr>
				<tr>
					<td><label for="idnumber">ID number*:</label></td>
					<td>
						<input type="text" name="idnumber" id="idnumber" value="<?php echo $order3['idnumber']; ?>" />
						<em>required by law, for ISP services</em>
					</td>
				</tr>
				<tr>
					<td><label for="vatnumber">VAT number:</label></td>
					<td>
						<input type="text" name="vatnumber" id="vatnumber" value="<?php echo $order3['vatnumber']; ?>" />
						<em>required by law for you to claim VAT</em>
					</td>
				</tr>
			</table>
			<table>	
				<tr>

					<td><input type="radio" name="debit_account" id="debit_account" value="debit_account" <?php if($order3['bank'] || $order3['account_number'] || $order3['bcode'] || $order3['type'] || $order3['holder']){ ?> checked  <?php } ?> style="width: auto;border: none;background: none;padding: 0;" /></td>
					<td><label for="debit_account">Please debit my account directly using a debit order:</label></td>
				</tr>
			</table>
			<table>					
				<tr>
					<td width="220px"><label for="bank">Bank:</label></td>
					<td><input type="text" name="bank" id="bank"  value="<?php echo $order3['bank'];?>"/></td>
				</tr>
				<tr>
					<td><label for="account_number">Account number:</label></td>
					<td><input type="text" name="account_number" id="account_number" value="<?php echo $order3['account_number'];?>"/></td>
				</tr>
				<tr>
					<td><label for="bcode">Branch code:</label></td>
					<td><input type="text" name="bcode" id="bcode" value="<?php echo $order3['bcode'];?>" /></td>
				</tr>
				<tr>
					<td>Account type:</td>
					<td>
						<select name="type">
								<option value="(select)">(select)</option>
								<option value="cheque" <?php if($order3['type']=='cheque'){ ?> selected  <?php } ?>>cheque</option>
								<option value="savings" <?php if($order3['type']=='savings'){ ?> selected  <?php } ?>>savings</option>
								<option value="transmission" <?php if($order3['type']=='transmission'){ ?> selected  <?php } ?>>transmission</option>
								<option value="baccount" <?php if($order3['type']=='baccount'){ ?> selected  <?php } ?>>bond account</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="holder">Holder</label></td>
					<td><input type="text" name="holder" id="holder"  value="<?php echo $order3['holder'];?>" /></td>
				</tr>
			</table>
			<p><strong>Or</strong></p>
			<table>
				<tr>
					<td><input type="radio" name="debit_account" id="debit_account"  value="regular_invoice" <?php if($order3['debit_account']=='regular_invoice'){ ?> checked  <?php } ?> style="width: auto;border: none;background: none;padding: 0;" <?php  ?> /></td>
					<td><label for="else">Please send me a regular invoice that includes Vanilla banking information and I will deposit the money directly into your account.</label></td>
				</tr>
			</table>
<!-- Begin reCaptcha -->

  <script type="text/javascript"
     src="http://www.google.com/recaptcha/api/challenge?k=6Ldpn98SAAAAACnEJE9uIffup7wHwEr9WvL5PHe0 ">
  </script>
  <noscript>
     <iframe src="http://www.google.com/recaptcha/api/noscript?k=6Ldpn98SAAAAACnEJE9uIffup7wHwEr9WvL5PHe0 "
         height="300" width="500" frameborder="0"></iframe><br>
     <textarea name="recaptcha_challenge_field" rows="3" cols="40">
     </textarea>
     <input type="hidden" name="recaptcha_response_field"
         value="manual_challenge">
  </noscript>

<!-- End reCaptcha -->
<br>
			<p style="display:inline;"><input class="order_button" style="border:1px solid #417c97;" type="submit" value="Submit"/>

<br>

</p>
	</form>
	</div>
<p>
<!-- Google Code for purchase Conversion Page -->
<script language="JavaScript" type="text/javascript">
<!--
var google_conversion_id = 1072595188;
var google_conversion_language = "en_US";
var google_conversion_format = "1";
var google_conversion_color = "666666";
if (1) {
  var google_conversion_value = 1;
}
var google_conversion_label = "purchase";
//-->
</script>
<script language="JavaScript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<img height=1 width=1 border=0 src="http://www.googleadservices.com/pagead/conversion/1072595188/imp.gif?value=1&label=purchase&script=0">
</noscript>
</p>
<?php
	footer();
?>
