<!DOCTYPE html>
<html lang="en">
<head>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script>
    $(function () {
        $('input.more').on('click', function () {
            var $table = $('#input_fields');
            var $tr = $table.find('tr').eq(0).clone();			
            $tr.appendTo($table).find('input').val('');
			var $tr2 = $table.find('tr').eq(1).clone();
			 $tr2.appendTo($table).find('input').val('');
        });
    });

</script>
<style type="text/css">
table {
width:500px;
table-layout:fixed;
}


td.linebreak p {
    width: 50%;
}

label, input {
    display: inline-block;
    vertical-align: baseline;
    width: 50%;
}



form, input {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
}

form {
    width: 500px;
}
</style>
</head>
<body>
<?php echo validation_errors('<p class="error">'); ?>
<?php echo form_open('site/add_job'); ?>

	<label for="reference_number">Refrence Number:</label><input type="text" id="reference_number" name = "reference_number">
    <label for="title">Job Title:</label><input type="text" id="title" name="title">
    <label for="description">Job Description :</label><input type="text" id="description" name="description">
    <label for="requirements">Job Requirements :</label><input type="text" id="requirements" name="requirements">
    <table id="input_fields">
        <tr> 
            <td>Question</td><td> <input type="text" name="question[]" /></td>             
        </tr>
		<tr>
			<td >Correct Response: </td><td><input type="text" name="response[]" /></td>
		</tr>
    </table>


<input class="more" type="button" value="Add more question" name="addmore" />
<input value="Complete" type="submit">
<?php echo form_close(); ?>
</body>
</html>