<?php

Class Corporate_model extends CI_Model {

	function add_jobs()

{
	$data['reference_number'] = $this->input->post('reference_number');
	$data['title'] = $this->input->post('title');
	$data['description'] = $this->input->post('description');
	$data['corporate_recruiter_id'] = 1;
	$data['requirements'] = $this->input->post('requirements');
	
	$data['date_listed'] = date('Y-m-d H:i:s', time()+7200);
	
	//$this->db->insert('corporate_job', $data);
	
	$questions = $this->input->post('question');
	
	$response = $this->input->post('response');
	
	
	

}	
	function add_recruiter()
		{
			$data = $this->input->post();
			return $this->db->insert('corporate_recruiter', $data);
		}
		
	public function generate_report()
	{
		$this->load->dbutil();
		$query_string = 'SELECT seeker.`first_name`,seeker.`last_name` FROM `seeker` LEFT JOIN apply ON seeker.id = apply.seeker_id where apply.is_successful = "y" and apply.job_id = 6';
		$query = $this->db->query($query_string);
		$delimiter = ",";
		
		$this->load->helper('file');
		$data = $this->dbutil->csv_from_result($query, $delimiter);
		
		
		
		if ( ! write_file('./uploads/file.csv', $data))
		{
			 echo 'Unable to write the file';
		}
		else
		{
			 echo 'File written!';
		}
		
	}
	
	public function get_email_details($job_id)
	{
		return $this
				->db
				->select('corporate_recruiter.email, corporate_recruiter.company_name,corporate_job.title,corporate_job.reference_number')
				->from('corporate_job')
				->join('corporate_recruiter', 'corporate_recruiter.id = corporate_job.corporate_recruiter_id' , 'left')
				->where('corporate_job.id',$job_id)
				->get()
				->row_array();
	}
	
	public function set_active_job($job_id)
	{
		$data = array('is_active' => 0);
		$this->db->where('id', $job_id);
		$this->db->update('corporate_job', $data); 
	}
	
	public function date_diff()
	{
		$query_string = 'SELECT id, DATEDIFF(CURDATE(),date_listed)as date FROM corporate_job WHERE date_emailed IS NULL';
		$query = $this->db->query($query_string);
		return $query->result_array();
	}
}