<?php

class Job_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function insert_batch($data)
	{
		$this->db->insert_batch('job', $data);
		
		return $this->db->affected_rows();
	}
}

